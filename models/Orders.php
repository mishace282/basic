<?php

namespace app\models;

use yii\db\ActiveRecord;
use \yii\db\Query;

use LisDev\Delivery\NovaPoshtaApi2;



class Orders extends ActiveRecord{
    public $id;
    public $ttn;
    public $status;   

    public static function tableName()
      {
             return 'orders'; // Имя таблицы в БД в которой хранятся записи блога
      }

      public static function UpdateO()
      {


$np = new NovaPoshtaApi2(
    'ad19f103d3b8fa6da805e2c163cd0b1b',
    'ru', // Язык возвращаемых данных: ru (default) | ua | en
    true, // При ошибке в запросе выбрасывать Exception: FALSE (default) | TRUE
    'curl' // Используемый механизм запроса: curl (defalut) | file_get_content
);

$tracks=array('20400134281273','20400134281253','20400133898495');
$data=array();
$status=array();
$status_ttn=array();
$kold=array();
//парсинг данных
for ($i=0; $i<count($tracks); $i++) {
    $date[$i] = strtr($np->documentsTracking($tracks[$i])['data'][0]['ScheduledDeliveryDate'], '/', '-');

    $data[$i]=date('Y-m-d', strtotime($date[$i]));
    $status_ttn[$i]=$np->documentsTracking($tracks[$i])['data'][0]['Status'];
    if ($status_ttn[$i]=="Нова пошта очікує надходження від відправника.")
           $status[$i]="Предотправка";
    elseif (strpos($status_ttn[$i],"Відправлення у місті.")!==false) 
           $status[$i]="Отправленно";
    elseif (strpos($status_ttn[$i],"Відправлення прямує")!==false) 
           $status[$i]="В отправлении";
    elseif (strpos($status_ttn[$i],"Відправлення отримано.")!==false) 
           $status[$i]="Получено";
    elseif ($status_ttn[$i]=="Прибув на відділення.")
           $status[$i]="Прибыла на Почту 1";
    else 
           $status[$i]="В обработке";
}

//print_r($np->documentsTracking("20400134281273")['data'][0]);
$bulkInsertArray = array();
//проверка, если есть то обновление, если нет то вставка
for ($i=0; $i<count($tracks); $i++) {

    if (!Orders::find()->where(['ttn' => $tracks[$i]])->asArray()->one()){
        $bulkInsertArray[]=[
       'ttn'=>$tracks[$i],
       'status'=>$status[$i],
       'status_ttn'=>$status_ttn[$i],
       'dat'=>$data[$i]
   ];
}

else {

     if (Orders::find()->where(['ttn'=>$tracks[$i], 'status' => "Предоправка"])->asArray()->one() && $status[$i]!="Нова пошта очікує надходження від відправника")
           \Yii::$app->db->createCommand("UPDATE orders SET status=:status, status_ttn=:status_ttn WHERE ttn=:ttn")
->bindValue(':status', "Отправленно")
->bindValue(':status_ttn', $status_ttn[$i])
->bindValue(':ttn',$tracks[$i])
->execute();


if (Orders::find()->where(['ttn'=>$tracks[$i], 'status' => "Отправленно"])->asArray()->one() && strpos($status[$i],"Відправлення у місті")!==false)
           \Yii::$app->db->createCommand("UPDATE orders SET status=:status, status_ttn=:status_ttn WHERE ttn=:ttn")
->bindValue(':status', "Прибыла на Почту 1")
->bindValue(':status_ttn', $status_ttn[$i])
->bindValue(':ttn',$tracks[$i])
->execute();

$kold[$i] = date_diff(date_create($data[$i]),date_create(date("Y-m-d")));
//echo "Количество дней : ". $kold[$i]->days;

if (Orders::find()->where(['ttn'=>$tracks[$i], 'status' => "Прибыла на Почту 1"])->asArray()->one() && $kold[$i]==2)
           \Yii::$app->db->createCommand("UPDATE orders SET status=:status WHERE ttn=:ttn")
->bindValue(':status', "Прибыла на Почту 2")
->bindValue(':ttn',$tracks[$i])
->execute();


if (Orders::find()->where(['ttn'=>$tracks[$i], 'status' => "Прибыла на Почту 2"])->asArray()->one() && $kold[$i]==3)
           \Yii::$app->db->createCommand("UPDATE orders SET status=:status WHERE ttn=:ttn")
->bindValue(':status', "Прибыла на Почту 3")
->bindValue(':ttn',$tracks[$i])
->execute();


if (Orders::find()->where(['ttn'=>$tracks[$i], 'status' => "Прибыла на Почту 3"])->asArray()->one() && $kold[$i]==4)
           \Yii::$app->db->createCommand("UPDATE orders SET status=:status WHERE ttn=:ttn")
->bindValue(':status', "Прибыла на Почту 4")
->bindValue(':ttn',$tracks[$i])
->execute();

if (Orders::find()->where(['ttn'=>$tracks[$i], 'status' => "Прибыла на Почту 3"])->asArray()->one() && $kold[$i]>=4)
           \Yii::$app->db->createCommand("UPDATE orders SET status=:status WHERE ttn=:ttn")
->bindValue(':status', "Прибыла на Почту 5")
->bindValue(':ttn',$tracks[$i])
->execute();


if (strpos($status[$i],"Відправлення отримано")!==false)
           \Yii::$app->db->createCommand("UPDATE orders SET status=:status, status_ttn=:status_ttn WHERE ttn=:ttn")
->bindValue(':status', "В пути домой (Деньги)")
->bindValue(':status_ttn', $status_ttn[$i])
->bindValue(':ttn',$tracks[$i])
->execute();



}


}


if(count($bulkInsertArray)>0){
    $columnNameArray=['ttn','status','status_ttn','dat'];
    // below line insert all your record and return number of rows inserted
    \Yii::$app->db->createCommand()
                   ->batchInsert(
                         "orders", $columnNameArray, $bulkInsertArray
                     )
                   ->execute();
}
        

           //$ords = Orders::find()->asArray()->all();

          /* $orders = Orders::find()->where(['id' =>1])->asArray()->one();
foreach ($ords as $val) {

    \Yii::$app->db->createCommand("UPDATE orders SET ttn=:ttn WHERE id=:id")
->bindValue(':id', $val['id'])
->bindValue(':ttn',1111)
->execute();
  }*/
 }
}
