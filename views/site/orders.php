<script> function autoReload() {
      location.reload(); 
}
setInterval("autoReload()",3600000);
//setInterval("autoReload()",10000);
</script>

<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

use app\models\Orders; 

use yii\grid\GridView;
use yii\data\ActiveDataProvider;

use yii\data\SqlDataProvider;



$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-orders">
    <h1><?= Html::encode($this->title) ?></h1>
<?
//header('Refresh:10');




   $count = Yii::$app->db->createCommand('
    SELECT COUNT(*) FROM orders')->queryScalar();
    
    $provider = new SqlDataProvider([
    'sql' => 'SELECT * FROM orders',
    'totalCount' => $count,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => [
            'id',
            'ttn',
            'status',
            'status_ttn',
            'dat',
        ],
    ],
]);

// возвращает массив данных
$models = $provider->getModels();

    //$model = $dataProvider->getModels();
echo GridView::widget([
    'dataProvider' => $provider,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
        	[
            'header' => 'ID',
            'attribute' => 'id',
        ],
        [
            'header' => 'ТТН',
            'attribute' => 'ttn',
        ],
          [
            'header' => 'Статус CRM',
            'attribute' => 'status',
        ],
        [
            'header' => 'Статус TTH',
            'attribute' => 'status_ttn',
        ],
         [
            'header' => 'Дата прибытия',
            'attribute' => 'dat',
            'format' => ['date', 'php:d/m/Y']
        ],

        ],

]);
    ?>

    <code><?= __FILE__ ?></code>
</div>
